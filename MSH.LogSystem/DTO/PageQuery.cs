﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PageQuery
    {
        public Pagination Pagination { get; set; } = new Pagination();
    }
}
