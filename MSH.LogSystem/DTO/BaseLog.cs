﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BaseLog
    {
        public string Id { get; set; }

        /// <summary>
        /// 日志级别
        /// </summary>
        public string LogLevelName { get; set; }
    }
}
