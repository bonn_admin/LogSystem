﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 日志总量报表
    /// </summary>
    public class TotalLogReport
    {
        /// <summary>
        /// Info日志总量
        /// </summary>
        public long InfoCount { get; set; }

        /// <summary>
        /// Warn日志总量
        /// </summary>
        public long WarnCount { get; set; }

        /// <summary>
        /// Debug日志总量
        /// </summary>
        public long DebugCount { get; set; }

        /// <summary>
        /// Error日志总量
        /// </summary>
        public long ErrorCount { get; set; }

        /// <summary>
        /// 所有日志总量
        /// </summary>
        public long TotalCount
        {
            get
            {
                return InfoCount + WarnCount + DebugCount + ErrorCount;
            }
        }
    }
}
