# 欢迎使用“妙生活日志中心系统”

## ClientReleases

[![NuGet](https://img.shields.io/nuget/v/MSH.LogClient.svg?style=plastic)](https://www.nuget.org/packages/MSH.LogClient/)

#### **项目介绍**

> 妙生活日志中心系统是上海妙尺商贸有限公司应对各种系统的日志集中式处理的解决方案，可以将多客户端、多系统的日志发送到日志处理中心服务器，对各个系统的日志进行集中式管理。

#### **使用前的准备**

1.**硬件准备**

 - Windows Server 2012 四核 8G 

2.**软件准备**

 - [RabbitMq（windows版本）](http://www.rabbitmq.com/news.html#2018-07-05T17:00:00+00:00)
 - [MongoDB（windows版本）](https://www.mongodb.com/download-center#enterprise)
 - .Net Framework 4.5.1

#### **软件架构**

>  [架构图](https://gitee.com/wuyuege/LogSystem/wikis/pages?title=%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84&parent=)


#### **安装教程**
>  [安装教程](https://gitee.com/wuyuege/LogSystem/wikis/pages?title=%E7%B3%BB%E7%BB%9F%E7%9A%84%E5%AE%89%E8%A3%85&parent=)
#### **使用说明**
1. 服务端配置用户后直接从管理端页面进入
2. 客户端使用：   
`MSHLogger.DefaultInfo("测试！");`    
或    
`MSHLogger.Instance("订单", "新建").Info("业务测试2");`   
具体用法请移步：[日志客户端的使用](https://gitee.com/wuyuege/LogSystem/wikis/pages?title=%E4%BD%BF%E7%94%A8%E6%8F%90%E4%BE%9B%E7%9A%84.Net%E5%AE%A2%E6%88%B7%E7%AB%AF&parent=)

#### **管理端预览**

![平台设置](https://images.gitee.com/uploads/images/2018/0815/114119_5d78c53d_767835.png "屏幕截图.png")
![日志查看](https://images.gitee.com/uploads/images/2018/0815/114209_cf6afdd8_767835.png "屏幕截图.png")

#### **参与贡献**

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### **感谢以下开源项目**
[iview-admin](https://github.com/iview/iview-admin)   
[SuperSocket](https://github.com/kerryjiang/SuperSocket)
